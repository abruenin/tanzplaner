alter table "SAAL" add
("SAA_MAX_PERSONS_NUM" NUMBER);

update saal set saa_max_persons_num = to_number(saa_max_persons);
commit;
select * from saal;

update saal set saa_max_persons = null;
commit;
select * from saal;

alter table "SAAL" modify
("SAA_MAX_PERSONS" NUMBER);

update saal set saa_max_persons = saa_max_persons_num;
commit;
select * from saal;

alter table saal drop COLUMN SAA_MAX_PERSONS_NUM;
select * from saal;