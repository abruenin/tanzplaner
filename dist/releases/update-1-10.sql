truncate table belegungs_vorlage;
truncate table belegungs_plan;

alter table belegungs_plan drop column bep_bev_von;

alter table belegungs_vorlage add (
    "BEV_START"     INTERVAL DAY(7) TO SECOND,
    "BEV_END"       INTERVAL DAY(7) TO SECOND
);

alter table belegungs_vorlage drop constraint BELEGUNGS_VORLAGE_CON;
alter table belegungs_vorlage drop column bev_von;
alter table belegungs_vorlage drop column bev_bis;

desc belegungs_plan
commit;

alter table belegungs_vorlage modify bev_start not null;
alter table belegungs_vorlage modify bev_end not null;

-- alter table belegungs_vorlage modify bev_start null;
-- alter table belegungs_vorlage modify bev_end null;

 alter table belegungs_vorlage add
 CONSTRAINT "BEV_END_GREATER_START" CHECK (bev_start < bev_end ) ENABLE;
 
 alter table BELEGUNGS_VORLAGE add
    BEV_BEP_ID_FK   number default belegungs_vorlage_seq.nextval;
 
 alter TABLE BELEGUNGS_VORLAGE 
    add FOREIGN KEY (BEV_BEP_ID_FK) references BELEGUNGS_PLAN(BEP_ID_PK);
