CREATE OR REPLACE FORCE EDITIONABLE VIEW "TANZPLANER"."THIS_WEEK_TEMPLATES" ("BEV_ID_PK", "BEV_BET_ID_FK", "BEV_NAME", "BEV_SAA_ID_FK", "BEV_START", "BEV_END", "BEV_BEP_ID_FK") DEFAULT COLLATION "USING_NLS_COMP"  AS 
  WITH week_start AS (
    select date_time_pkg.get_monday_midnight_for(sysdate) as week_start from dual
)
SELECT 
    BEV_ID_PK,
    BEV_BET_ID_FK,
    BEV_NAME,
    BEV_SAA_ID_FK,
    BEV_START,
    BEV_END,
    BEV_BEP_ID_FK 
FROM 
    BELEGUNGS_VORLAGE, 
    week_start;


alter table BELEGUNGS_VORLAGE 
    rename constraint SYS_C0077098
    to BEV_BEP_FK_CON;
  
alter table BELEGUNGS_VORLAGE 
    rename constraint ZEITEN_PK1
    to BELEGUNGS_VORLAGE_PK;
  

alter index ZT_UNIQUE_CON RENAME to BET_NAME_UNIQUE_IDX;

