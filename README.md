# Tanzplaner Project

Tanzplaner ist eine Anwendung für Tanzsportvereine zur Verwaltung der Saalbelegungen. Es können regelmässige Kurse ebenso wie freies Training und Privatstunden verwaltet werden. Ursprünglich wurde Tanzplaner während der CoVid-Pandemie entwickelt, um das freie Training der Paare zu organisieren.
Tanzplaner ist entwickelt mit Oracle Application Express (APEX) für Oracle Datenbanken. Es kann auf der kostenlosen Oracle Express Edition (XE) oder auf einer kostenlosen Oracle Autonomous Database auf einem Free-Tier in der Oracle Cloud Infrastructure (OCI) deployed werden. 
Tanzplaner kann frei genutzt und weitergegeben werden.

