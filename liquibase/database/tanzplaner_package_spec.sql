CREATE OR REPLACE EDITIONABLE PACKAGE "TANZPLANER" IS

    c_default_days_to_purge constant number := 90;

    FUNCTION max_persons_per_saal (
        p_saa_id_in saal.saa_id_pk%TYPE
    ) RETURN PLS_INTEGER;

    FUNCTION booked_persons_per_saalbelegung (
        p_sab_id_in saalbelegung.sab_id_pk%TYPE
    ) RETURN PLS_INTEGER;

    PROCEDURE copy_template_for_week (
        bep_id_pk_in   IN number,
        target_date_in IN DATE
    );

	function get_ben_id_from_name (
		p_ben_name_in	varchar2 
		) return number;

	function user_already_booked_for_name (
		p_ben_name_in	in	varchar2,
		p_sab_id_in		in number
	) return boolean;

	procedure book_user (
			p_ben_name_in	in benutzer.ben_name%TYPE,
			p_sab_id_in		in saalbelegung.sab_id_pk%TYPE 
	);
    procedure book_private_lesson(
			p_ben_id_in         in number,
            p_trainer_name_in   in varchar2,
			p_sab_id_in		    in number 
	);

    procedure delete_older_than (
        p_age_days      in number   default c_default_days_to_purge
    );

    PROCEDURE create_saalbelegung(
        p_startdate_in  in date,
        p_enddate_in    in date,
        p_saal_id_in    in number
    );

    PROCEDURE create_vorlage(
        p_startdate_in  in date,
        p_enddate_in    in date,
        p_saal_id_in    in number,
        p_plan_id_in    in number
    );

    PROCEDURE create_vorlage_interval(
        p_startdate_in  in INTERVAL DAY TO SECOND,
        p_enddate_in    in INTERVAL DAY TO SECOND,
        p_saal_id_in    in number,
        p_plan_id_in    in number
    );


    procedure move_saalbelegung(
        p_sab_id_in     in  number,
        p_startdate_in  in date,
        p_enddate_in    in date
    );

    procedure move_vorlage(
        p_bev_id_in     in  number,
        p_startdate_in  in date,
        p_enddate_in    in date
    );

    PROCEDURE auto_apply_plan;

/*    
    function private_lesson_already_booked( 
        p_ben_id_in         number, 
        p_trainer_name_in   varchar2, 
        sdb_sab_id_fk       number
    ) return boolean;
*/          
/*
    function get_monday_date_for(
        p_date_in       in date
    ) return date;
*/
/*
    function get_monday_midnight_for(
        p_date_in       in date
    ) return date;
*/
/*
    function get_monday_midnight
    return date;
**
/*
    function date_is_monday(
        p_date_in       in date
    ) return boolean;
*/
/*
    function date_is_monday_number(
        p_date_in       in date
    ) return number;
*/
/*
    function create_week_interval_from_date (
        p_date_in IN  DATE
    ) return INTERVAL DAY TO SECOND;

    function create_week_interval_from_datestring (
        p_string_in IN VARCHAR2
    ) return INTERVAL DAY TO SECOND;
*/

END TANZPLANER;
/