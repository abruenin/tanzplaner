CREATE OR REPLACE EDITIONABLE TRIGGER "SAALBELEGUNG_BENUTZER_TRG" BEFORE
    INSERT ON saalbelegung_benutzer
    FOR EACH ROW
BEGIN

-- RAISE_APPLICATION_ERROR(-20000, 'FIRED -->>> TRIGGER SAALBELEGUNG_BENUTZER_TRG');
-- RAISE_APPLICATION_ERROR(-20000, 'INSERTING -->>> TRIGGER SAALBELEGUNG_BENUTZER_TRG INSERTING');

    << column_sequences >> BEGIN
        IF
            inserting
            AND :new.sbe_ben_id_fk IS NULL
        THEN
            SELECT
                saalbelegung_benutzer_seq.NEXTVAL
            INTO :new.sbe_ben_id_fk
            FROM
                sys.dual;
        END IF;
        
        IF INSERTING AND :NEW.SBE_ID_PK IS NULL THEN
            SELECT SAALBELEGUNG_BENUTZER_SEQ.NEXTVAL INTO :NEW.SBE_ID_PK FROM SYS.DUAL;
        END IF;

    END column_sequences;
END;
/
