CREATE OR REPLACE EDITIONABLE PACKAGE "DATE_TIME_PKG" IS

    function get_monday_date_for(
        p_date_in       in date
    ) return date;

    function get_monday_midnight
    return date;

    function get_monday_midnight_for(
    p_date_in       in date
    ) return date;

    function create_week_interval_from_date (
        p_date_in IN  DATE
    ) return INTERVAL DAY TO SECOND;

    -- procedure convert_to_isodate(
    --     p_date_in       in  DATE,
    --     p_isodate_out   out varchar2
    -- );

end DATE_TIME_PKG;
/