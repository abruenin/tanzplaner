CREATE OR REPLACE FORCE EDITIONABLE VIEW "THIS_WEEK_TEMPLATES" ("BEV_ID_PK", "BEV_BET_ID_FK", "BEV_NAME", "BEV_SAA_ID_FK", "BEV_START", "BEV_END", "BEV_BEP_ID_FK") AS 
  WITH week_start AS (
    select date_time_pkg.get_monday_midnight_for(sysdate) as week_start from dual
)
SELECT 
    BEV_ID_PK,
    BEV_BET_ID_FK,
    BEV_NAME,
    BEV_SAA_ID_FK,
    BEV_START,
    BEV_END,
    BEV_BEP_ID_FK 
FROM 
    BELEGUNGS_VORLAGE, 
    week_start;