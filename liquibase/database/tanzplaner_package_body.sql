CREATE OR REPLACE EDITIONABLE PACKAGE BODY "TANZPLANER" IS

    ----- Booking Couples ------------------------------------------------

    FUNCTION max_persons_per_saal (
        p_saa_id_in saal.saa_id_pk%TYPE
    ) RETURN PLS_INTEGER IS
        max_person_cnt  PLS_INTEGER;
    BEGIN

        select  saa_max_persons
        into    max_person_cnt
        from    saal
        where   saa_id_pk = p_saa_id_in;

        return max_person_cnt;
    END max_persons_per_saal;


    FUNCTION booked_persons_per_saalbelegung (
        p_sab_id_in saalbelegung.sab_id_pk%TYPE
    ) RETURN PLS_INTEGER IS
        act_person_cnt  PLS_INTEGER;
    BEGIN
        select	count(sbe_ben_id_fk)*2 + count(sbe_ben_trainer_id_fk) personen_im_saal
        into    act_person_cnt
        from	SAALBELEGUNG_BENUTZER
        where   sbe_sab_id_fk = p_sab_id_in;

        -- raise_application_error( -20001, 'act_person_cnt >' || act_person_cnt || '<');

        return act_person_cnt;
    END;


    FUNCTION get_ben_id_from_name (
        p_ben_name_in VARCHAR2
    ) RETURN NUMBER IS
        ben_id NUMBER;
    BEGIN
        SELECT
            ben_id_pk
        INTO ben_id
        FROM
            benutzer
        WHERE
            lower(ben_name) = lower(p_ben_name_in);

        RETURN ben_id;
    END get_ben_id_from_name;


    FUNCTION user_already_booked_for_id (
        p_ben_id_in  IN  NUMBER
      , p_sab_id_in  IN  NUMBER
    ) RETURN BOOLEAN IS
        found NUMBER;
    BEGIN
        SELECT
            COUNT(*)
        INTO found
        FROM
            saalbelegung_benutzer
        WHERE
                sbe_ben_id_fk = p_ben_id_in
            AND sbe_sab_id_fk = p_sab_id_in;

        RETURN found > 0;
    END user_already_booked_for_id;


    FUNCTION user_already_booked_for_name (
        p_ben_name_in  IN  VARCHAR2
      , p_sab_id_in    IN  NUMBER
    ) RETURN BOOLEAN IS
    BEGIN
        RETURN user_already_booked_for_id(get_ben_id_from_name(p_ben_name_in), p_sab_id_in);
    END;


    PROCEDURE book_user (
        p_ben_name_in  IN benutzer.ben_name%TYPE
      , p_sab_id_in    IN saalbelegung.sab_id_pk%TYPE
    ) IS
        ben_id  benutzer.ben_id_pk%TYPE;
        saa_id  saal.saa_id_pk%TYPE;
    BEGIN
        ben_id := get_ben_id_from_name(p_ben_name_in);

        select  sab_saa_id_fk
        into    saa_id
        from    saalbelegung
        where   sab_id_pk = p_sab_id_in;

                -- raise_application_error( -20001, 'Benutzer >' || p_ben_name_in || '< ID >' || ben_id || '<' || 'sab_id >' || p_sab_id_in || '< saa_id >' || saa_id || '<');

        debug_pkg.assert(NOT user_already_booked_for_id(ben_id, p_sab_id_in)
                        , 'Paar schon gebucht für diese Zeit');
        debug_pkg.assert( booked_persons_per_saalbelegung(p_sab_id_in)+1 < max_persons_per_saal(saa_id) -- booked+2 <= max
                        , 'Saal darf nicht überbucht werden');

        INSERT INTO saalbelegung_benutzer (
            sbe_ben_id_fk
            , sbe_sab_id_fk
        ) VALUES (
            ben_id
          , p_sab_id_in
        );

        COMMIT;
    END book_user;


    PROCEDURE copy_template_for_week (
        bep_id_pk_in   IN  number
      , target_date_in IN  DATE
    ) IS
        week_start DATE;
    BEGIN
        debug_pkg.assert(to_char(target_date_in, 'fmday', 'nls_date_language=GERMAN') = 'montag', 'Das Zieldatum muß ein Montag sein >>'
                                                                   || bep_id_pk_in
                                                                   || '<<');

        week_start := date_time_pkg.get_monday_midnight_for( target_date_in );

        -- raise_application_error( -20001, 'Von >' || bep_id_pk_in || '< Nach >' || to_char( week_start, 'YYYY-MM-DD HH24:MI:SS' ) || '<');

        -- first delete all entries from a plan in this week 
        DELETE FROM saalbelegung 
            WHERE sab_bev_id_fk is not null
            AND   sab_von >= date_time_pkg.get_monday_midnight_for( target_date_in )
            AND   sab_bis < date_time_pkg.get_monday_midnight_for( target_date_in )+7;

        -- then insert new entries from given plan for that week
        INSERT INTO saalbelegung (
            sab_von
        , sab_bis
        , sab_bet_id_fk
        , sab_saa_id_fk
        , sab_name
        , sab_max_persons
        , sab_bev_id_fk
        )
        WITH target_week_start AS (
            select date_time_pkg.get_monday_midnight_for (target_date_in) as target_week_start from dual
        )
            SELECT
            bev_start + target_week_start
            , bev_start + target_week_start
            , bev_bet_id_fk
            , bev_saa_id_fk
            , bev_name
            , saa_max_persons
            , bev_id_pk
        FROM
            belegungs_vorlage
            , saal
            , target_week_start
        WHERE
            bev_saa_id_fk = saa_id_pk
        ;

    END copy_template_for_week;


    FUNCTION private_lesson_already_booked (
        p_ben_id_in        IN  NUMBER
      , p_trainer_name_in  IN  VARCHAR2
      , p_sab_id_in        IN  NUMBER
    ) RETURN BOOLEAN IS
        found NUMBER;
    BEGIN
        SELECT
            COUNT(*)
        INTO found
        FROM
            saalbelegung_benutzer
        WHERE
                sbe_ben_trainer_id_fk = get_ben_id_from_name(p_trainer_name_in)
            AND sbe_sab_id_fk = p_sab_id_in;

        RETURN found > 0;
    END;


    PROCEDURE book_private_lesson (
        p_ben_id_in        IN  NUMBER
      , p_trainer_name_in  IN  VARCHAR2
      , p_sab_id_in        IN  NUMBER
    ) IS
        trainer_id NUMBER;
    BEGIN
        debug_pkg.assert(NOT user_already_booked_for_id(p_ben_id_in, p_sab_id_in)
                       , 'Paar schon gebucht für diese Zeit');
        debug_pkg.assert(NOT private_lesson_already_booked(p_ben_id_in, p_trainer_name_in, p_sab_id_in)
                       , 'Diese Privatstunde wurde schon gebucht');
        trainer_id := get_ben_id_from_name(p_trainer_name_in);

        -- raise_application_error( -20001, 'ben_id >' || p_ben_id_in || '< trainer_name >' || p_trainer_name_in || '< sab_id >' || p_sab_id_in || '<' );

        INSERT INTO saalbelegung_benutzer (
            sbe_ben_id_fk
            , sbe_sab_id_fk
            , sbe_ben_trainer_id_fk
        ) VALUES (
            p_ben_id_in
          , p_sab_id_in
          , trainer_id
        );

        COMMIT;
    END book_private_lesson;


    PROCEDURE create_saalbelegung (
        p_startdate_in  IN  DATE
      , p_enddate_in    IN  DATE
      , p_saal_id_in    in  number
    ) IS
    BEGIN
        /*
        raise_application_error(-20999, 'Entry created >>>' 
            || to_char( p_startdate_in, 'DD.MM.YYYY HH24:MI') || '<<< >>>'
            || to_char( p_enddate_in, 'DD.MM.YYYY HH24:MI') || '<<< >>>' 
            || p_saal_id_in || '<<<' );
        */
        /**/
        INSERT INTO saalbelegung (
            sab_von
            , sab_bis
            , sab_bet_id_fk
            , sab_saa_id_fk
            ) VALUES (
              p_startdate_in
            , p_enddate_in
            , 1 -- BET_ID = 1 --> Freies Training
            , p_saal_id_in
        );
        /**/
    END create_saalbelegung;


    PROCEDURE create_vorlage (
        p_startdate_in  IN  DATE
      , p_enddate_in    IN  DATE
      , p_saal_id_in    in  number
      , p_plan_id_in    in number
    ) IS
        interval_start  INTERVAL DAY TO SECOND;
        interval_end    INTERVAL DAY TO SECOND;
    BEGIN
        interval_start  := numtodsinterval( p_startdate_in - date_time_pkg.get_monday_midnight, 'DAY' );
        interval_end    := numtodsinterval( p_enddate_in - date_time_pkg.get_monday_midnight, 'DAY' );

        -- DBMS_OUTPUT.PUT_LINE('istart >' || interval_start || ' iend >' || interval_end );

        INSERT INTO belegungs_vorlage (
              bev_start
            , bev_end
            , bev_bet_id_fk
            , bev_saa_id_fk
            , bev_bep_id_fk
            ) VALUES (
              interval_start
            , interval_end
            , 1 -- BET_ID = 1 --> Freies Training
            , p_saal_id_in
            , p_plan_id_in
        );

    END create_vorlage;


    PROCEDURE create_vorlage_interval (
        p_startdate_in  IN  INTERVAL DAY TO SECOND
      , p_enddate_in    IN  INTERVAL DAY TO SECOND
      , p_saal_id_in    in  number
      , p_plan_id_in    in  number   
    ) IS
    BEGIN

        INSERT INTO belegungs_vorlage (
              bev_start
            , bev_end
            , bev_bet_id_fk
            , bev_saa_id_fk
            , bev_bep_id_fk
            ) VALUES (
              p_startdate_in
            , p_enddate_in
            , 1 -- BET_ID = 1 --> Freies Training
            , p_saal_id_in
            , p_plan_id_in
        );

    END create_vorlage_interval;

    procedure move_saalbelegung(
         p_sab_id_in    in  number
       , p_startdate_in  IN  DATE
       , p_enddate_in    IN  DATE
    ) is
    begin

        UPDATE saalbelegung
        SET
          sab_von = p_startdate_in
        , sab_bis = p_enddate_in
        WHERE
        sab_id_pk = p_sab_id_in;

    end move_saalbelegung;    


    procedure move_vorlage(
         p_bev_id_in    in  number
       , p_startdate_in IN  DATE
       , p_enddate_in   IN  DATE
    ) is
    begin

/*
        raise_application_error(-20999, 'Entry moved >>> Start=' 
            || to_char( p_startdate_in, 'DD.MM.YYYY HH24:MI') || '<<< >>> End='
            || to_char( p_enddate_in, 'DD.MM.YYYY HH24:MI') || '<<< >>>' 
            || ' Interval >>>'
            || create_week_interval_from_date( p_startdate_in ) || '<<< >>> End='
            || create_week_interval_from_date( p_enddate_in ) || '<<< >>>' 
            || p_bev_id_in || '<<<' );
*/
        UPDATE belegungs_vorlage
        SET
          bev_start = date_time_pkg.create_week_interval_from_date( p_startdate_in )
        , bev_end   = date_time_pkg.create_week_interval_from_date( p_enddate_in )
        WHERE
        bev_id_pk = p_bev_id_in;

        commit;

    end move_vorlage;


    PROCEDURE delete_older_than (
        p_age_days IN NUMBER DEFAULT c_default_days_to_purge
    ) IS
    BEGIN
        -- raise_application_error( -20001, 'p_age_days >' || p_age_days || '<' );

        IF p_age_days < c_default_days_to_purge THEN
            raise_application_error(-20001, 'p_age_days >'
                                            || p_age_days
                                            || '< Beim Löschen müssen mindestens '
                                            || 90
                                            || ' Tage verbleiben');
        END IF;

        DELETE FROM saalbelegung
        WHERE
            trunc(sab_von, 'DD') < trunc(sysdate, 'DD') - p_age_days;

    END;


    PROCEDURE auto_apply_plan IS
        cursor cur_plan is 
            select  bep_id_pk
            from    belegungs_plan
            where   bep_auto_apply = 'Y';

        target_monday   date;

    BEGIN

        target_monday := trunc( date_time_pkg.get_monday_date_for( SYSDATE+7 ), 'DD' );
        dbms_output.put_line( '--->' || to_char( target_monday,'DD.MM.YYYY HH24:MI') );

        APEX_AUTOMATION.LOG_INFO( 'Target_Monday: ' || target_monday );


        for r_plan in cur_plan loop
            APEX_AUTOMATION.LOG_INFO( 'Plan ID: ' || r_plan.bep_id_pk );
            copy_template_for_week( r_plan.bep_id_pk, target_monday );          
        end loop;

        commit;

    END auto_apply_plan;

/*
    FUNCTION get_monday_date_for (
        p_date_in IN DATE
    ) RETURN DATE IS
    BEGIN
        RETURN next_day((p_date_in - 7), 'monday');
    END get_monday_date_for;
*/

    /*
    function get_monday_midnight_for(
        p_date_in       in date
    ) return date is
    begin
        return trunc(p_date_in, 'iw');
    end get_monday_midnight_for;
    */

    /*
    function get_monday_midnight
        return date is
    begin
        return trunc(sysdate, 'iw');
    end get_monday_midnight;
    */

/*
    FUNCTION date_is_monday (
        p_date_in DATE
    ) RETURN BOOLEAN IS
        retval BOOLEAN;
    BEGIN

        IF p_date_in IS NULL THEN
            retval := false;
        ELSE
            retval := ( p_date_in = tanzplaner.get_monday_date_for(p_date_in) );
        END IF;

        RETURN retval;
    END date_is_monday;
*/
/*
    FUNCTION date_is_monday_number (
        p_date_in IN DATE
    ) RETURN NUMBER IS
    BEGIN
        RETURN
            CASE
                WHEN date_is_monday(p_date_in) THEN
                    1
                ELSE 0
            END;
    END date_is_monday_number;
*/
    /*
    function create_week_interval_from_date(
        p_date_in IN  DATE
    ) return INTERVAL DAY TO SECOND is
        -- retval INTERVAL DAY TO SECOND;
    begin
        return numtodsinterval( p_date_in - date_time_pkg.get_monday_midnight, 'DAY' );

        -- return retval;
    end ;
    */

    /*
    function create_week_interval_from_datestring(
        p_string_in IN  VARCHAR2
    ) return INTERVAL DAY TO SECOND is
        -- dateval DATE;
        -- retval INTERVAL DAY TO SECOND;
    begin
        -- retval := numtodsinterval( p_date_in-get_monday_midnight, 'DAY' );
        -- dateval := to_date(p_string_in, 'DD.MM.YYYY HH24:MI:SS' );
        -- return create_week_interval_from_date(dateval);

        return create_week_interval_from_date( to_date(p_string_in, 'DD.MM.YYYY HH24:MI:SS' ) );
    end ;
*/


END tanzplaner;
/