CREATE OR REPLACE EDITIONABLE TRIGGER "BENUTZER_BIU" 
    before insert or update  
    on "BENUTZER" 
    for each row 
begin

    if :new.ben_id_pk is null then 
        :new.ben_id_pk := to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'); 
    end if; 

    if :new.ben_password is not null then
        :new.ben_password := login_pkg.obfuscate(text_in => :new.ben_password);
    end if;

end benutzer_biu;
/
