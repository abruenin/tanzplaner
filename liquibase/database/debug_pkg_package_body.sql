CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DEBUG_PKG" AS

    procedure raise_user_error(in_error in pls_integer, in_error_msg in varchar2 ) is
        type error_tab_t is table of varchar2(128 char)
           index by pls_integer;
        error_tab error_tab_t;
    begin
        error_tab(20999) := 'Assertion failed!';
        
        raise_application_error(in_error, error_tab(in_error) || ' ' || in_error_msg);
    end raise_user_error;

    PROCEDURE assert (
        condition  IN  BOOLEAN
      , message    IN  VARCHAR2
    ) AS
    -- e_assertion_failed   EXCEPTION;
    -- PRAGMA EXCEPTION_INIT ( e_assertion_failed, -20999 );
    BEGIN
        IF NOT condition THEN
            raise_user_error(1, message);
            -- raise_application_error(-20999, 'Assertion failed! ' || message);
        END IF;
    END assert;
  
  
    PROCEDURE record_error IS
        PRAGMA autonomous_transaction;
        l_code  PLS_INTEGER := sqlcode;
        -- l_mesg  VARCHAR2(32767 char) := sqlerrm;
        l_mesg error_log.error_message%type;
    BEGIN
        INSERT INTO error_log (
            error_code
            , error_message
            , backtrace
            , callstack
            , created_on
            , created_by
        ) VALUES (
            l_code
          , l_mesg
          , sys.dbms_utility.format_error_backtrace
          , sys.dbms_utility.format_call_stack
          , sysdate
          , user
        );

        COMMIT;
    END record_error;

END debug_pkg;
/