CREATE OR REPLACE EDITIONABLE PACKAGE BODY "LOGIN_PKG" IS

    FUNCTION obfuscate (
        text_in IN VARCHAR2
    ) RETURN RAW IS
        l_returnvalue RAW(16);
    BEGIN
        dbms_obfuscation_toolkit.md5(input => sys.utl_raw.cast_to_raw(text_in), checksum => l_returnvalue);

        RETURN l_returnvalue;
    END obfuscate;

    FUNCTION auth (
        p_username  IN  VARCHAR2,
        p_password  IN  VARCHAR2
    ) RETURN BOOLEAN IS
        l_obfuscated_password  benutzer.ben_password%TYPE;
        l_value                NUMBER;
        l_returnvalue          BOOLEAN;
    BEGIN
        l_obfuscated_password := obfuscate(text_in => p_password);
        
        << tryGetOneUser >>
        BEGIN
            SELECT
                1
            INTO l_value
            FROM
                benutzer
            WHERE
                    upper(benutzer.ben_name) = upper(p_username)
                AND benutzer.ben_password = l_obfuscated_password;

        EXCEPTION
            WHEN no_data_found OR too_many_rows THEN
                l_value := 0;
            WHEN OTHERS THEN
                l_value := 0;
        END tryGetOneUser;

        l_returnvalue := l_value = 1;
        RETURN l_returnvalue;
    END auth;

/*
    PROCEDURE change_password (
        p_username  IN  VARCHAR2,
        p_password  IN  VARCHAR2
    ) IS
        l_obfuscated_password benutzer.ben_password%TYPE;
    BEGIN      
        -- DBMS_OUTPUT.PUT_LINE( 'PWDCHANGE >>>' || p_username || '<<<>>>' || p_password || '<<<');

        -- l_obfuscated_password := obfuscate(text_in => p_password);     
        UPDATE benutzer
        SET
            ben_password = p_password -- l_obfuscated_password
        WHERE
            ben_name = lower(p_username);

        COMMIT;
    END change_password;
*/

    FUNCTION pwd_complexity_matched (
        p_pwd_in in VARCHAR2
    ) RETURN BOOLEAN IS
        retval BOOLEAN := true;
    BEGIN
        retval :=
            retval
            AND ( regexp_instr(p_pwd_in, '[0-9]') > 0 ); -- at least one number
        retval :=
            retval
            AND ( regexp_instr(p_pwd_in, '[a-z]') > 0 ); -- at least one small letter
        retval :=
            retval
            AND ( regexp_instr(p_pwd_in, '[A-Z]') > 0 ); -- at least one large letter
        retval :=
            retval
            AND ( regexp_instr(p_pwd_in, '[!$%&/()=?;:_,\.-\#]') > 0 ); -- at least one special
        retval :=
            retval
            AND ( length(p_pwd_in) >= 10 ); -- at least 10 characters
        RETURN retval;
    END pwd_complexity_matched;

    FUNCTION get_ben_id_from_name (
        p_ben_name_in in VARCHAR2
    ) RETURN NUMBER IS
        ben_id NUMBER;
    BEGIN
        << tryGetOneUser >>
        begin
            SELECT
                ben_id_pk
            INTO ben_id
            FROM
                benutzer
            WHERE
                lower(ben_name) = lower(p_ben_name_in);

            EXCEPTION
                WHEN no_data_found OR too_many_rows THEN
                    ben_id := 0;
                WHEN OTHERS THEN
                    ben_id := 0;
        end tryGetOneUser;

        RETURN ben_id;
    END get_ben_id_from_name;

/*
    PROCEDURE add_user (
        p_username_in  IN  VARCHAR2,
        p_password_in  IN  VARCHAR2
    ) IS
        l_obfuscated_password benutzer.ben_password%TYPE;
    BEGIN
        l_obfuscated_password := obfuscate(text_in => p_password_in);
        INSERT INTO benutzer (
            ben_id_pk,
            ben_name,
            ben_password
        ) VALUES (
            NULL,
            p_username_in,
            l_obfuscated_password
        );
    END adduser;
*/

END login_pkg;
/