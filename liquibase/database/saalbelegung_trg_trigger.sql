CREATE OR REPLACE EDITIONABLE TRIGGER "SAALBELEGUNG_TRG" BEFORE
    INSERT ON saalbelegung
    FOR EACH ROW
BEGIN
    << column_sequences >> BEGIN
        IF
            inserting
            AND :new.sab_id_pk IS NULL
        THEN
            SELECT
                saalbelegung_seq.NEXTVAL
            INTO :new.sab_id_pk
            FROM
                sys.dual;

        END IF;
    END column_sequences;
END;
/
