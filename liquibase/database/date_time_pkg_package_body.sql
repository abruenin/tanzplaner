CREATE OR REPLACE EDITIONABLE PACKAGE BODY "DATE_TIME_PKG" IS

    -- Makro in 23ai
    FUNCTION get_monday_date_for (
        p_date_in IN DATE
    ) RETURN DATE IS
    BEGIN
        RETURN next_day((p_date_in - 7), 'monday');
    END get_monday_date_for;

    -- Makro in 23ai
    -- Boolean in 23ai
    FUNCTION date_is_monday (
        p_date_in DATE
    ) RETURN BOOLEAN IS
        retval BOOLEAN;
    BEGIN

        IF p_date_in IS NULL THEN
            retval := false;
        ELSE
            retval := ( p_date_in = get_monday_date_for(p_date_in) );
        END IF;

        RETURN retval;
    END date_is_monday;

    -- Makro in 23ai
    -- Boolean in 23ai
    FUNCTION date_is_monday_number (
        p_date_in IN DATE
    ) RETURN NUMBER IS
    BEGIN
        RETURN
            CASE
                WHEN date_is_monday(p_date_in) THEN
                    1
                ELSE 0
            END;
    END date_is_monday_number;


    -- Makro in 23ai
    function get_monday_midnight_for(
        p_date_in       in date
    ) return date is
    begin
        return trunc(p_date_in, 'iw');
    end get_monday_midnight_for;


    -- MAKRO with 23c
    function get_monday_midnight
        return date is
    begin
        return trunc(sysdate, 'iw');
    end get_monday_midnight;

    -- Makro in 23ai
    function create_week_interval_from_date(
        p_date_in IN  DATE
    ) return INTERVAL DAY TO SECOND is
        -- retval INTERVAL DAY TO SECOND;
    begin
        return numtodsinterval( p_date_in - date_time_pkg.get_monday_midnight, 'DAY' );

        -- return retval;
    end ;


    -- Makro in 23ai
    function create_week_interval_from_datestring(
        p_string_in IN  VARCHAR2
    ) return INTERVAL DAY TO SECOND is
        -- dateval DATE;
        -- retval INTERVAL DAY TO SECOND;
    begin
        -- retval := numtodsinterval( p_date_in-get_monday_midnight, 'DAY' );
        -- dateval := to_date(p_string_in, 'DD.MM.YYYY HH24:MI:SS' );
        -- return create_week_interval_from_date(dateval);

        return create_week_interval_from_date( to_date(p_string_in, 'DD.MM.YYYY HH24:MI:SS' ) );
    end ;


    /*
    PROCEDURE convert_to_isodate (
        p_date_in      IN   DATE
      , p_isodate_out  OUT  VARCHAR2
    ) IS
    BEGIN
        SELECT
            to_char(p_date_in, 'YYYY-MM-DD')
        INTO p_isodate_out
        FROM
            dual;

    END convert_to_isodate;
    */


end DATE_TIME_PKG;
/