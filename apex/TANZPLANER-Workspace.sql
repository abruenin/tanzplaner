prompt --application/set_environment
set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_200200 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2020.10.01'
,p_default_workspace_id=>100000
);
end;
/
prompt  WORKSPACE 100000
--
-- Workspace, User Group, User, and Team Development Export:
--   Date and Time:   21:39 Monday February 8, 2021
--   Exported By:     TANZPLANER
--   Export Type:     Workspace Export
--   Version:         20.2.0.00.20
--   Instance ID:     9946240919280116
--
-- Import:
--   Using Instance Administration / Manage Workspaces
--   or
--   Using SQL*Plus as the Oracle user APEX_200200
 
begin
    wwv_flow_api.set_security_group_id(p_security_group_id=>100000);
end;
/
----------------
-- W O R K S P A C E
-- Creating a workspace will not create database schemas or objects.
-- This API creates only the meta data for this APEX workspace
prompt  Creating workspace TANZPLANER...
begin
wwv_flow_fnd_user_api.create_company (
  p_id => 9946860130669658
 ,p_provisioning_company_id => 100000
 ,p_short_name => 'TANZPLANER'
 ,p_display_name => 'TANZPLANER'
 ,p_first_schema_provisioned => 'TANZPLANER'
 ,p_company_schemas => 'TANZPLANER'
 ,p_account_status => 'ASSIGNED'
 ,p_allow_plsql_editing => 'Y'
 ,p_allow_app_building_yn => 'Y'
 ,p_allow_packaged_app_ins_yn => 'Y'
 ,p_allow_sql_workshop_yn => 'Y'
 ,p_allow_websheet_dev_yn => 'Y'
 ,p_allow_team_development_yn => 'Y'
 ,p_allow_to_be_purged_yn => 'Y'
 ,p_allow_restful_services_yn => 'Y'
 ,p_source_identifier => 'TANZPLAN'
 ,p_webservice_logging_yn => 'Y'
 ,p_path_prefix => 'TANZPLANER'
 ,p_files_version => 1
);
end;
/
----------------
-- G R O U P S
--
prompt  Creating Groups...
begin
wwv_flow_fnd_user_api.create_user_group (
  p_id => 1201797923825920,
  p_GROUP_NAME => 'OAuth2 Client Developer',
  p_SECURITY_GROUP_ID => 10,
  p_GROUP_DESC => 'Users authorized to register OAuth2 Client Applications');
end;
/
begin
wwv_flow_fnd_user_api.create_user_group (
  p_id => 1201653558825920,
  p_GROUP_NAME => 'RESTful Services',
  p_SECURITY_GROUP_ID => 10,
  p_GROUP_DESC => 'Users authorized to use RESTful Services with this workspace');
end;
/
begin
wwv_flow_fnd_user_api.create_user_group (
  p_id => 1201546103825918,
  p_GROUP_NAME => 'SQL Developer',
  p_SECURITY_GROUP_ID => 10,
  p_GROUP_DESC => 'Users authorized to use SQL Developer with this workspace');
end;
/
prompt  Creating group grants...
----------------
-- U S E R S
-- User repository for use with APEX cookie-based authentication.
--
prompt  Creating Users...
begin
wwv_flow_fnd_user_api.create_fnd_user (
  p_user_id                      => '9946602789669424',
  p_user_name                    => 'TANZPLANER',
  p_first_name                   => '',
  p_last_name                    => '',
  p_description                  => '',
  p_email_address                => '',
  p_web_password                 => '4981C338A35005E1740E3814E3C36BECB34F4328B1CEB83E7C94132038E2E43B7BEA07971AC4AFFA34496E0C5C906C5FF0843041262367D190632AF0F7F972C8',
  p_web_password_format          => '5;5;10000',
  p_group_ids                    => '1201546103825918:1201653558825920:1201797923825920:',
  p_developer_privs              => 'ADMIN:CREATE:DATA_LOADER:EDIT:HELP:MONITOR:SQL',
  p_default_schema               => 'TANZPLANER',
  p_account_locked               => 'N',
  p_account_expiry               => to_date('202006130733','YYYYMMDDHH24MI'),
  p_failed_access_attempts       => 0,
  p_change_password_on_first_use => 'N',
  p_first_password_use_occurred  => 'Y',
  p_allow_app_building_yn        => 'Y',
  p_allow_sql_workshop_yn        => 'Y',
  p_allow_websheet_dev_yn        => 'Y',
  p_allow_team_development_yn    => 'Y',
  p_allow_access_to_schemas      => '');
end;
/
prompt Check Compatibility...
begin
-- This date identifies the minimum version required to import this file.
wwv_flow_team_api.check_version(p_version_yyyy_mm_dd=>'2010.05.13');
end;
/
 
begin wwv_flow.g_import_in_progress := true; wwv_flow.g_user := USER; end; 
/
 
--
prompt ...feedback
--
begin
null;
end;
/
--
prompt ...Issue Templates
--
begin
null;
end;
/
--
prompt ...Issue Email Prefs
--
begin
wwv_flow_team_api.create_issue_email_prefs (
  p_id => 14104456717689807 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_receive_emails_yn => 'Y'
 ,p_notification_types => 'ISSUE_EDIT:COMMENT_ADD:COMMENT_EDIT:STATUS:ASSIGNEE:SUBSCRIBER:MILESTONE:LABEL:DUPLICATE:ASSOCIATION:ATTACHMENT'
 ,p_frequency => 'IMMEDIATELY'
 ,p_created_on => to_date('20210207132243','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132243','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
end;
/
--
prompt ...Label Groups
--
begin
null;
end;
/
--
prompt ...Labels
--
begin
null;
end;
/
--
prompt ... Milestones
--
begin
wwv_flow_team_api.create_milestone (
  p_id => 14104273161686839 + wwv_flow_team_api.g_id_offset
 ,p_milestone_name => 'Migrate Dev to FreeTier - 02.01.21'
 ,p_milestone_date => to_date('20210212000000','YYYYMMDDHH24MISS')
 ,p_milestone_status => 'OPEN'
 ,p_milestone_slug => 'migrate-dev-to-freetier-02-01-21'
 ,p_milestone_id => 1
 ,p_created_on => to_date('20210207132213','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132213','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_milestone (
  p_id => 14108166526720591 + wwv_flow_team_api.g_id_offset
 ,p_milestone_name => 'Freigabe Trainer'
 ,p_milestone_date => to_date('20210228000000','YYYYMMDDHH24MISS')
 ,p_milestone_status => 'OPEN'
 ,p_milestone_slug => 'freigabe-trainer'
 ,p_milestone_id => 2
 ,p_created_on => to_date('20210207132751','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132751','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
end;
/
--
prompt ... Issues
--
begin
wwv_flow_team_api.create_issue (
  p_id => 14104380326689805 + wwv_flow_team_api.g_id_offset
 ,p_title => 'TTC-Workspace auf FreeTier anlegen'
 ,p_slug => 'ttc-workspace-auf-freetier-anlegen'
 ,p_issue_text => 
'TTC-Workspace auf FreeTier anlegen'
 ,p_row_version => 2
 ,p_status => 'CLOSED'
 ,p_issue_number => 1
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132243','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132301','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14105063852694070 + wwv_flow_team_api.g_id_offset
 ,p_title => 'Tabellen nach Schema Tanzplaner'
 ,p_slug => 'tabellen-nach-schema-tanzplaner'
 ,p_issue_text => 
'Tabellen nach Schema Tanzplaner'
 ,p_row_version => 2
 ,p_status => 'CLOSED'
 ,p_issue_number => 2
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132326','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132345','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14105522774697194 + wwv_flow_team_api.g_id_offset
 ,p_title => 'Umstellen auf Sequence/Trigger'
 ,p_slug => 'umstellen-auf-sequence-trigger'
 ,p_issue_text => 
'Umstellen auf Sequence/Trigger'
 ,p_row_version => 2
 ,p_status => 'CLOSED'
 ,p_issue_number => 3
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132357','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132417','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14106018320700422 + wwv_flow_team_api.g_id_offset
 ,p_title => 'Package umbenennen und nach Schema Tanzplaner'
 ,p_slug => 'package-umbenennen-und-nach-schema-tanzplaner'
 ,p_issue_text => 
'Package umbenennen und nach Schema Tanzplaner'
 ,p_row_version => 2
 ,p_status => 'CLOSED'
 ,p_issue_number => 4
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132429','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132437','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14106568027702315 + wwv_flow_team_api.g_id_offset
 ,p_title => 'Tinyurl to FreeTier TTC'
 ,p_slug => 'tinyurl-to-freetier-ttc'
 ,p_issue_text => 
'Tinyurl to FreeTier TTC'
 ,p_row_version => 1
 ,p_status => 'OPEN'
 ,p_issue_number => 5
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132448','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132448','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14106825673704186 + wwv_flow_team_api.g_id_offset
 ,p_title => 'Application to FreeTier TTC'
 ,p_slug => 'application-to-freetier-ttc'
 ,p_issue_text => 
'Application to FreeTier TTC'
 ,p_row_version => 1
 ,p_status => 'OPEN'
 ,p_issue_number => 6
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132507','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132507','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14107168011707218 + wwv_flow_team_api.g_id_offset
 ,p_title => 'DB-Export nach TTC FreeTier'
 ,p_slug => 'db-export-nach-ttc-freetier'
 ,p_issue_text => 
'DB-Export nach TTC FreeTier'
 ,p_row_version => 1
 ,p_status => 'OPEN'
 ,p_issue_number => 7
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132537','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132537','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14107493979708956 + wwv_flow_team_api.g_id_offset
 ,p_title => 'PLSQL-Export nach TTC-FreeTier'
 ,p_slug => 'plsql-export-nach-ttc-freetier'
 ,p_issue_text => 
'PLSQL-Export nach TTC-FreeTier'
 ,p_row_version => 1
 ,p_status => 'OPEN'
 ,p_issue_number => 8
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132555','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132555','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14108221431722514 + wwv_flow_team_api.g_id_offset
 ,p_title => 'Gruppe aufteilen für Trainer'
 ,p_slug => 'gruppe-aufteilen-für-trainer'
 ,p_issue_text => 
'Gruppe aufteilen für Trainer'
 ,p_row_version => 1
 ,p_status => 'OPEN'
 ,p_issue_number => 9
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132810','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132810','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14108550381724078 + wwv_flow_team_api.g_id_offset
 ,p_title => 'Kalender klick erzeugt Eintrag an Datum/Uhrzeit'
 ,p_slug => 'kalender-klick-erzeugt-eintrag-an-datum-uhrzeit'
 ,p_issue_text => 
'Kalender klick erzeugt Eintrag an Datum/Uhrzeit'
 ,p_row_version => 1
 ,p_status => 'OPEN'
 ,p_issue_number => 10
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132826','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132826','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14108879783726714 + wwv_flow_team_api.g_id_offset
 ,p_title => 'Trainer extra eintragen'
 ,p_slug => 'trainer-extra-eintragen'
 ,p_issue_text => 
'Trainer extra eintragen'
 ,p_row_version => 1
 ,p_status => 'OPEN'
 ,p_issue_number => 11
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132852','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132852','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue (
  p_id => 14109154962728245 + wwv_flow_team_api.g_id_offset
 ,p_title => 'Assert'
 ,p_slug => 'assert'
 ,p_issue_text => 
'Assert'
 ,p_row_version => 1
 ,p_status => 'OPEN'
 ,p_issue_number => 12
 ,p_deleted => 'N'
 ,p_created_on => to_date('20210207132908','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132908','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
end;
/
--
prompt ... Issue Attachments
--
begin
null;
end;
/
--
prompt ... Issues Milestones
--
begin
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14104380326689805 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14104273161686839 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132256','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132256','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14105063852694070 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14104273161686839 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132335','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132335','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14105522774697194 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14104273161686839 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132414','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132414','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14106018320700422 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14104273161686839 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132435','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132435','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14106568027702315 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14104273161686839 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132455','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132455','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14106825673704186 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14104273161686839 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132516','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132516','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14107168011707218 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14104273161686839 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132546','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132546','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14107493979708956 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14104273161686839 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132603','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132603','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14108221431722514 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14108166526720591 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132815','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132815','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14108550381724078 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14108166526720591 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132832','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132832','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14108879783726714 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14108166526720591 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132859','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132859','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_milestone (
  p_issue_id => 14109154962728245 + wwv_flow_team_api.g_id_offset
 ,p_milestone_id => 14108166526720591 + wwv_flow_team_api.g_id_offset
 ,p_created_on => to_date('20210207132925','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132925','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
end;
/
--
prompt ... Issues Labels
--
begin
null;
end;
/
--
prompt ... Issues stakeholders
--
begin
null;
end;
/
--
prompt ... Issues Comments
--
begin
null;
end;
/
--
prompt ... Issues Events
--
begin
wwv_flow_team_api.create_issue_event (
  p_id => 14104625955691060 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14104380326689805 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132256','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:22:56.139500000+00:00", "details": [{ "name": "Migrate Dev to FreeTier - 02.01.21", "id": "14104273161686839", "slug": "migrate-dev-to-freetier-02-01-21"}]}'
 ,p_created_on => to_date('20210207132256','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132256','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14104848822691619 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14104380326689805 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132301','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "status", "action": "set", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:23:01.751224000+00:00", "details": [{ "name": "CLOSED"}]}'
 ,p_created_on => to_date('20210207132301','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132301','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14105176386694950 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14105063852694070 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132335','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:23:35.053178000+00:00", "details": [{ "name": "Migrate Dev to FreeTier - 02.01.21", "id": "14104273161686839", "slug": "migrate-dev-to-freetier-02-01-21"}]}'
 ,p_created_on => to_date('20210207132335','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132335','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14105379012695949 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14105063852694070 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132345','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "status", "action": "set", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:23:45.052480000+00:00", "details": [{ "name": "CLOSED"}]}'
 ,p_created_on => to_date('20210207132345','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132345','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14105671531698937 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14105522774697194 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132414','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:24:14.929624000+00:00", "details": [{ "name": "Migrate Dev to FreeTier - 02.01.21", "id": "14104273161686839", "slug": "migrate-dev-to-freetier-02-01-21"}]}'
 ,p_created_on => to_date('20210207132414','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132414','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14105853342699158 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14105522774697194 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132417','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "status", "action": "set", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:24:17.134724000+00:00", "details": [{ "name": "CLOSED"}]}'
 ,p_created_on => to_date('20210207132417','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132417','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14106118280701043 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14106018320700422 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132435','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:24:35.982601000+00:00", "details": [{ "name": "Migrate Dev to FreeTier - 02.01.21", "id": "14104273161686839", "slug": "migrate-dev-to-freetier-02-01-21"}]}'
 ,p_created_on => to_date('20210207132435','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132435','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14106383163701233 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14106018320700422 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132437','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "status", "action": "set", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:24:37.885646000+00:00", "details": [{ "name": "CLOSED"}]}'
 ,p_created_on => to_date('20210207132437','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132437','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14106622474703004 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14106568027702315 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132455','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:24:55.599618000+00:00", "details": [{ "name": "Migrate Dev to FreeTier - 02.01.21", "id": "14104273161686839", "slug": "migrate-dev-to-freetier-02-01-21"}]}'
 ,p_created_on => to_date('20210207132455','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132455','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14106972724705135 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14106825673704186 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132516','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:25:16.911929000+00:00", "details": [{ "name": "Migrate Dev to FreeTier - 02.01.21", "id": "14104273161686839", "slug": "migrate-dev-to-freetier-02-01-21"}]}'
 ,p_created_on => to_date('20210207132516','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132516','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14107269820708048 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14107168011707218 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132546','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:25:46.032210000+00:00", "details": [{ "name": "Migrate Dev to FreeTier - 02.01.21", "id": "14104273161686839", "slug": "migrate-dev-to-freetier-02-01-21"}]}'
 ,p_created_on => to_date('20210207132546','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132546','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14107575545709766 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14107493979708956 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132603','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:26:03.213412000+00:00", "details": [{ "name": "Migrate Dev to FreeTier - 02.01.21", "id": "14104273161686839", "slug": "migrate-dev-to-freetier-02-01-21"}]}'
 ,p_created_on => to_date('20210207132603','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132603','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14108316236722981 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14108221431722514 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132815','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:28:15.369920000+00:00", "details": [{ "name": "Freigabe Trainer", "id": "14108166526720591", "slug": "freigabe-trainer"}]}'
 ,p_created_on => to_date('20210207132815','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132815','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14108690475724664 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14108550381724078 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132832','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:28:32.199876000+00:00", "details": [{ "name": "Freigabe Trainer", "id": "14108166526720591", "slug": "freigabe-trainer"}]}'
 ,p_created_on => to_date('20210207132832','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132832','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14108993626727368 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14108879783726714 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132859','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:28:59.237379000+00:00", "details": [{ "name": "Freigabe Trainer", "id": "14108166526720591", "slug": "freigabe-trainer"}]}'
 ,p_created_on => to_date('20210207132859','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132859','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
wwv_flow_team_api.create_issue_event (
  p_id => 14109230048729950 + wwv_flow_team_api.g_id_offset
 ,p_issue_id => 14109154962728245 + wwv_flow_team_api.g_id_offset
 ,p_event_date => to_date('20210207132925','YYYYMMDDHH24MISS')
 ,p_event_text => 
'{"attribute": "milestone","action": "add", "performer": "TANZPLANER", "performedOn": "2021-02-07T13:29:25.058999000+00:00", "details": [{ "name": "Freigabe Trainer", "id": "14108166526720591", "slug": "freigabe-trainer"}]}'
 ,p_created_on => to_date('20210207132925','YYYYMMDDHH24MISS')
 ,p_created_by => 'TANZPLANER'
 ,p_updated_on => to_date('20210207132925','YYYYMMDDHH24MISS')
 ,p_updated_by => 'TANZPLANER'
);
end;
/
--
prompt ... Issues Notifications
--
begin
wwv_flow_team_api.create_issue_notification (
  p_id => 14104757769691063 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14104380326689805 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14104625955691060 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132256.187643000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14104902955691619 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14104380326689805 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14104848822691619 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'STATUS'
 ,p_created_on => to_timestamp_tz('20210207132301.752538000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14105257160694950 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14105063852694070 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14105176386694950 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132335.055331000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14105413201695950 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14105063852694070 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14105379012695949 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'STATUS'
 ,p_created_on => to_timestamp_tz('20210207132345.053803000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14105712098698937 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14105522774697194 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14105671531698937 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132414.931808000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14105969554699158 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14105522774697194 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14105853342699158 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'STATUS'
 ,p_created_on => to_timestamp_tz('20210207132417.135892000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14106224111701043 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14106018320700422 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14106118280701043 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132435.984629000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14106460936701233 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14106018320700422 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14106383163701233 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'STATUS'
 ,p_created_on => to_timestamp_tz('20210207132437.886699000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14106746038703004 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14106568027702315 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14106622474703004 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132455.601454000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14107012018705136 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14106825673704186 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14106972724705135 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132516.913846000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14107396566708048 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14107168011707218 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14107269820708048 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132546.034164000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14107642381709766 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14107493979708956 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14107575545709766 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132603.215276000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14108433122722981 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14108221431722514 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14108316236722981 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132815.371833000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14108741123724664 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14108550381724078 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14108690475724664 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132832.201772000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14109091258727368 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14108879783726714 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14108993626727368 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132859.240378000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
wwv_flow_team_api.create_issue_notification (
  p_id => 14109326777729950 + wwv_flow_team_api.g_id_offset
 ,p_user_id => 'TANZPLANER'
 ,p_issue_id => 14109154962728245 + wwv_flow_team_api.g_id_offset
 ,p_event_id => 14109230048729950 + wwv_flow_team_api.g_id_offset
 ,p_read_yn => 'N'
 ,p_type => 'MILESTONE'
 ,p_created_on => to_timestamp_tz('20210207132925.060834000 +00:00 ','YYYYMMDDHH24MISSxFF TZR TZD')
 ,p_email_sent_yn => 'N'
);
end;
/
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
