create or replace force editionable view "TANZPLANER"."THIS_WEEK_TEMPLATES" (
   "BEV_ID_PK",
   "BEV_BET_ID_FK",
   "BEV_NAME",
   "BEV_SAA_ID_FK",
   "BEV_START",
   "BEV_END",
   "BEV_BEP_ID_FK"
) as
   with week_start as (
      select tanzplaner.get_monday_midnight_for(sysdate) as week_start
        from dual
   )
   select bev_id_pk,
          bev_bet_id_fk,
          bev_name,
          bev_saa_id_fk,
          bev_start,
          bev_end,
          bev_bep_id_fk
     from belegungs_vorlage,
          week_start;