create table "TANZPLANER"."SAAL" (
   "SAA_ID_PK"       number,
   "SAA_NAME"        varchar2(64 byte) not null enable,
   "SAA_MAX_PERSONS" number,
   constraint "SAAL_PK" primary key ( "SAA_ID_PK" )
      using index enable
);


create or replace editionable trigger "TANZPLANER"."SAAL_TRG" before
   insert on saal
   for each row
begin
   << column_sequences >> begin
      null;
   end column_sequences;
end;
/
alter trigger "TANZPLANER"."SAAL_TRG" enable;