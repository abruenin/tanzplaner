create table "TANZPLANER"."BENUTZER" (
   "BEN_ID_PK"      number not null enable,
   "BEN_NAME"       varchar2(255 byte) not null enable,
   "BEN_PASSWORD"   varchar2(4000 byte) not null enable,
   "BEN_BGR_ID_FK"  number not null enable,
   "BEN_IS_TRAINER" number,
   constraint "BENUTZER_PK" primary key ( "BEN_ID_PK" )
      using index enable,
   constraint "BENUTZER_UK1" unique ( "BEN_NAME" )
      using index enable,
   constraint "BEN_BGR_FK" foreign key ( "BEN_BGR_ID_FK" )
      references "TANZPLANER"."BENUTZERGRUPPE" ( "BGR_ID_PK" )
   enable
);


create or replace editionable trigger "TANZPLANER"."BENUTZER_BIU" before
   insert or update on "BENUTZER"
   for each row
begin
   if :new.ben_id_pk is null then
      :new.ben_id_pk := to_number ( sys_guid(),'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' );
   end if;

   if :new.ben_password is not null then
      :new.ben_password := login_pkg.obfuscate(text_in => :new.ben_password);
   end if;

end benutzer_biu;
/
alter trigger "TANZPLANER"."BENUTZER_BIU" enable;