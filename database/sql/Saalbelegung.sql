create table "TANZPLANER"."SAALBELEGUNG" (
   "SAB_VON"         date,
   "SAB_BIS"         date,
   "SAB_NAME"        varchar2(100 byte),
   "SAB_BET_ID_FK"   number,
   "SAB_ID_PK"       number,
   "SAB_SAA_ID_FK"   number,
   "SAB_MAX_PERSONS" varchar2(20 byte),
   "SAB_BEV_ID_FK"   number,
   constraint "SAALBELEGUNG_PK" primary key ( "SAB_ID_PK" )
      using index enable,
   constraint "SAB_BET_FK" foreign key ( "SAB_BET_ID_FK" )
      references "TANZPLANER"."BELEGUNGSTYP" ( "BET_ID_PK" )
   enable,
   constraint "SAB_SAA_FK" foreign key ( "SAB_SAA_ID_FK" )
      references "TANZPLANER"."SAAL" ( "SAA_ID_PK" )
   enable
);


create or replace editionable trigger "TANZPLANER"."SAALBELEGUNG_TRG" before
   insert on saalbelegung
   for each row
begin
   << column_sequences >> begin
      if
         inserting
         and :new.sab_id_pk is null
      then
         select saalbelegung_seq.nextval
           into :new.sab_id_pk
           from sys.dual;

      end if;
   end column_sequences;
end;
/
alter trigger "TANZPLANER"."SAALBELEGUNG_TRG" enable;