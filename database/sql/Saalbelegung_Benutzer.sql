create table "TANZPLANER"."SAALBELEGUNG_BENUTZER" (
   "SBE_BEN_ID_FK"         number not null enable,
   "SBE_SAB_ID_FK"         number not null enable,
   "SBE_ID_PK"             number,
   "SBE_BEN_TRAINER_ID_FK" number,
   constraint "SAALBELEGUNG_BENUTZER_PK" primary key ( "SBE_ID_PK" )
      using index enable,
   constraint "SAALBELEGUNG_BENUTZER_BEN_FK" foreign key ( "SBE_BEN_ID_FK" )
      references "TANZPLANER"."BENUTZER" ( "BEN_ID_PK" )
         on delete cascade
   enable,
   constraint "SAALBELEGUNG_BENUTZER_SAB_FK" foreign key ( "SBE_SAB_ID_FK" )
      references "TANZPLANER"."SAALBELEGUNG" ( "SAB_ID_PK" )
         on delete cascade
   enable,
   constraint "SBE_BEN_TRAINER_ID_FK_CON" foreign key ( "SBE_BEN_TRAINER_ID_FK" )
      references "TANZPLANER"."BENUTZER" ( "BEN_ID_PK" )
   enable
);


create or replace editionable trigger "TANZPLANER"."SAALBELEGUNG_BENUTZER_TRG" before
   insert on saalbelegung_benutzer
   for each row
begin

-- RAISE_APPLICATION_ERROR(-20000, 'FIRED -->>> TRIGGER SAALBELEGUNG_BENUTZER_TRG');
-- RAISE_APPLICATION_ERROR(-20000, 'INSERTING -->>> TRIGGER SAALBELEGUNG_BENUTZER_TRG INSERTING');

   << column_sequences >> begin
      if
         inserting
         and :new.sbe_ben_id_fk is null
      then
         select saalbelegung_benutzer_seq.nextval
           into :new.sbe_ben_id_fk
           from sys.dual;
      end if;

      if
         inserting
         and :new.sbe_id_pk is null
      then
         select saalbelegung_benutzer_seq.nextval
           into :new.sbe_id_pk
           from sys.dual;
      end if;

   end column_sequences;
end;
/
alter trigger "TANZPLANER"."SAALBELEGUNG_BENUTZER_TRG" enable;