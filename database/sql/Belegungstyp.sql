create table "TANZPLANER"."BELEGUNGSTYP" (
   "BET_ID_PK" number,
   "BET_NAME"  varchar2(80 byte),
   "BET_FARBE" varchar2(64 byte),
   constraint "ZEITTYP_PK" primary key ( "BET_ID_PK" )
      using index enable,
   constraint "ZT_UNIQUE_CON"
      unique ( "BET_NAME" )
         using index (
            create unique index "TANZPLANER"."BET_NAME_UNIQUE_IDX" on
               "TANZPLANER"."BELEGUNGSTYP" (
                  "BET_NAME"
               )
         ) enable
);


create or replace editionable trigger "TANZPLANER"."BELEGUNGSTYP_TRG" before
   insert on belegungstyp
   for each row
begin
   << column_sequences >> begin
      if
         inserting
         and :new.bet_id_pk is null
      then
         select belegungstyp_seq.nextval
           into :new.bet_id_pk
           from sys.dual;
      end if;
   end column_sequences;
end;
/
alter trigger "TANZPLANER"."BELEGUNGSTYP_TRG" enable;