create table "TANZPLANER"."BELEGUNGS_VORLAGE" (
   "BEV_ID_PK"     number,
   "BEV_BET_ID_FK" number not null enable,
   "BEV_NAME"      varchar2(64 byte),
   "BEV_SAA_ID_FK" number not null enable,
   "BEV_START"     interval day(7) to second(6),
   "BEV_END"       interval day(7) to second(6),
   "BEV_BEP_ID_FK" number default "TANZPLANER"."BELEGUNGS_VORLAGE_SEQ"."NEXTVAL",
   constraint "BELEGUNGS_VORLAGE_PK"
      primary key ( "BEV_ID_PK" )
         using index (
            create unique index "TANZPLANER"."ZEITEN_PK1" on
               "TANZPLANER"."BELEGUNGS_VORLAGE" (
                  "BEV_ID_PK"
               )
         ) enable,
   constraint "BEV_END_GREATER_START" check ( bev_start < bev_end ) enable,
   constraint "BEV_BET_FK" foreign key ( "BEV_BET_ID_FK" )
      references "TANZPLANER"."BELEGUNGSTYP" ( "BET_ID_PK" )
   enable,
   constraint "BEV_SAA_FK" foreign key ( "BEV_SAA_ID_FK" )
      references "TANZPLANER"."SAAL" ( "SAA_ID_PK" )
   enable,
   constraint "BEV_BEP_FK_CON" foreign key ( "BEV_BEP_ID_FK" )
      references "TANZPLANER"."BELEGUNGS_PLAN" ( "BEP_ID_PK" )
   enable
);


create or replace editionable trigger "TANZPLANER"."BELEGUNGS_VORLAGE_TRG" before
   insert on belegungs_vorlage
   for each row
begin
   << column_sequences >> begin
      if
         inserting
         and :new.bev_id_pk is null
      then
         select belegungs_vorlage_seq.nextval
           into :new.bev_id_pk
           from sys.dual;
      end if;
   end column_sequences;
end;
/
alter trigger "TANZPLANER"."BELEGUNGS_VORLAGE_TRG" enable;