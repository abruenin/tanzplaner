create table "TANZPLANER"."BELEGUNGS_PLAN" (
   "BEP_ID_PK"        number not null enable,
   "BEP_NAME"         varchar2(64 byte) not null enable,
   "BEP_BESCHREIBUNG" varchar2(250 byte),
   "BEP_AUTO_APPLY"   char(1 byte),
   constraint "BELEGUNGS_PLAN_PK" primary key ( "BEP_ID_PK" )
      using index enable
);


create or replace editionable trigger "TANZPLANER"."BELEGUNGS_PLAN_TRG" before
   insert on belegungs_plan
   for each row
begin
   << column_sequences >> begin
      if
         inserting
         and :new.bep_id_pk is null
      then
         select belegungs_plan_seq.nextval
           into :new.bep_id_pk
           from sys.dual;
      end if;
   end column_sequences;
end;
/
alter trigger "TANZPLANER"."BELEGUNGS_PLAN_TRG" enable;