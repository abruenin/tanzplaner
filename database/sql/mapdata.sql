create table "TANZPLANER"."MAPDATA" (
   "MAP_ID"        number not null enable,
   "MAP_LONG"      number not null enable,
   "MAP_LAT"       number not null enable,
   "MAP_TEXT"      varchar2(32000 byte),
   "MAP_SIZE"      number(1,0),
   "MAP_IS_ROOT"   number(1,0),
   "MAP_TITLE"     varchar2(128 byte),
   "MAP_OSM_LINK"  varchar2(300 byte),
   "MAP_HERE_LINK" varchar2(300 byte),
   constraint "MAPDATA_PK" primary key ( "MAP_ID" )
      using index enable
);

