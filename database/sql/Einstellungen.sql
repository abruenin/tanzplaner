create table "TANZPLANER"."EINSTELLUNGEN" (
   "EIN_NAME"    varchar2(32 byte) not null enable,
   "EIN_NUMBER"  number,
   "EIN_VARCHAR" varchar2(32 byte),
   "EIN_ID_PK"   number default "TANZPLANER"."EINSTELLUNGEN_SEQ"."NEXTVAL" not null enable,
   constraint "EINSTELLUNGEN_PK" primary key ( "EIN_ID_PK" )
      using index enable
);


create or replace editionable trigger "TANZPLANER"."EINSTELLUNGEN_TRG" before
   insert on einstellungen
   for each row
begin
   << column_sequences >> begin
      if
         inserting
         and :new.ein_name is null
      then
         select einstellungen_seq.nextval
           into :new.ein_name
           from sys.dual;
      end if;
      if
         inserting
         and :new.ein_id_pk is null
      then
         select einstellungen_seq.nextval
           into :new.ein_id_pk
           from sys.dual;
      end if;
   end column_sequences;
end;
/
alter trigger "TANZPLANER"."EINSTELLUNGEN_TRG" enable;