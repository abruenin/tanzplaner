create table "TANZPLANER"."ERROR_LOG" (
   "ERROR_CODE"    number(*,0),
   "ERROR_MESSAGE" varchar2(4000 byte),
   "BACKTRACE"     clob,
   "CALLSTACK"     clob,
   "CREATED_ON"    date,
   "CREATED_BY"    varchar2(30 byte)
);
