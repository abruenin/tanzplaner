create or replace PACKAGE login_pkg IS

    FUNCTION obfuscate (
        text_in IN VARCHAR2
    ) RETURN RAW;

    FUNCTION auth (
        p_username  IN  VARCHAR2,
        p_password  IN  VARCHAR2
    ) RETURN BOOLEAN;

/*
    PROCEDURE change_password (
        p_username  IN  VARCHAR2,
        p_password  IN  VARCHAR2
    );
*/

    FUNCTION pwd_complexity_matched (
        p_pwd_in in VARCHAR2
    ) RETURN BOOLEAN;

    FUNCTION get_ben_id_from_name (
        p_ben_name_in in VARCHAR2
    ) RETURN NUMBER;

/*  
    PROCEDURE add_user (
        p_username_in  IN  VARCHAR2,
        p_password_in  IN  VARCHAR2
    );
*/

END login_pkg;