create or replace PACKAGE debug_pkg AS
    
    procedure raise_user_error (
        in_error        in  pls_integer, 
        in_error_msg    in  varchar2 
    );
    
    PROCEDURE assert (
        condition  IN  BOOLEAN
      , message    IN  VARCHAR2
    );

    PROCEDURE record_error;

END debug_pkg;