insert into BELEGUNGS_PLAN (BEP_ID_PK,BEP_NAME,BEP_BESCHREIBUNG,BEP_AUTO_APPLY) values 
    ('1','Standard-Plan','Der normale Plan','Y'),
    ('2','Ferienplan','Der Plan für die Ferien','N'),
    ('3','Pandemie-Plan','Wollen wir echt nicht wieder','N');


alter SEQUENCE BELEGUNGSTYP_SEQ INCREMENT by 3;
select BELEGUNGSTYP_SEQ.nextval;
alter SEQUENCE BELEGUNGSTYP_SEQ INCREMENT by 1;
