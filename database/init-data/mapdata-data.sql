insert into tanzplaner.mapdata (
   map_id,
   map_long,
   map_lat,
   map_text,
   map_size,
   map_is_root,
   map_title,
   map_osm_link,
   map_here_link
) values ( '2',
           '9,65808',
           '53,72597',
           'Das Sparkassenpokal-Turnier findet hier stadt

Schulstraße 30
25336 Klein Nordende',
           null,
           null,
           'Bürgermeister Hell Halle',
           'https://www.openstreetmap.org/way/60362561',
           'https://share.here.com/p/s-Yz1lZHVjYXRpb24tZmFjaWxpdHk7aWQ9Mjc2dTF3Y2UtNGY1MWM0YTNkY2FmNGNkMmI0NjVmZjUzYmExMDJhZDI7bGF0PTUzLjcyNjIxO2xvbj05LjY1ODQ1O249R3J1bmRzY2h1bGUrS2xlaW4rTm9yZGVuZGUrTGlldGg7bmxhdD01My43MjYwNztubG9uPTkuNjU4NTI7cGg9JTJCNDk0MTIxOTQzOTU7aD01NjRjNzk'
           );
insert into tanzplaner.mapdata (
   map_id,
   map_long,
   map_lat,
   map_text,
   map_size,
   map_is_root,
   map_title,
   map_osm_link,
   map_here_link
) values ( '1',
           '9,64688',
           '53,72255',
           'Hier trainieren wir',
           null,
           '1',
           'Tanz-Turnier-Club Elmshorn',
           'https://www.openstreetmap.org/way/123102085',
           'https://share.here.com/p/s-Yz1idWlsZGluZztsYXQ9NTMuNzIyNjM7bG9uPTkuNjQ3MDU7bj1TYW5kd2VnKzY0O2g9MTU3NDBj' );