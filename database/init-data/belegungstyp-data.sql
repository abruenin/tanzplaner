insert into BELEGUNGSTYP (BET_ID_PK,BET_NAME,BET_FARBE) values 
    ('1','Freies Training','apex-cal-green'),
    ('2','Gruppentraining','apex-cal-blue'),
    ('3','Veranstaltung','apex-cal-orange'),
    ('4','Sperrzeit','apex-cal-gray'),
    ('5','Wechsel','apex-cal-gray');

alter SEQUENCE BELEGUNGSTYP_SEQ INCREMENT by 5;
select BELEGUNGSTYP_SEQ.nextval;
alter SEQUENCE BELEGUNGSTYP_SEQ INCREMENT by 1;